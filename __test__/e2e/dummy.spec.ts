import gql from 'graphql-tag';
import { createSplitLink, http, websocket } from './utils/link';
import { Client } from './utils/client';

const link = createSplitLink(http, websocket);
const client = new Client(link);

test('query', async () => {
  const result = await client.query({
    query: gql`
      {
        hello
      }
    `
  });
  expect(result.data.hello).toEqual('Hello');
});

test('mutation', async () => {
  const result = await client.mutate({
    query: gql`
      mutation {
        publish
      }
    `
  });
  expect(result.data.publish).toEqual('value');
});

test('subscription generator', async () => {
  const result: any = await client.subscribe({
    query: gql`
      subscription {
        greetings
      }
    `
  });
  expect(result.data.greetings).toEqual('Hi');
});

test('subscription pubsub', async () => {
  const result: any = await client.subscribe({
    query: gql`
      subscription {
        subscribe
      }
    `,
    trigger: async () => {
      await client.mutate({
        query: gql`
          mutation {
            publish
          }
        `
      });
    }
  });
  expect(result.data.subscribe).toEqual('value');
});
