import { print } from 'graphql';
import {
  ApolloLink,
  Operation,
  FetchResult,
  Observable
} from '@apollo/client/core';
import {
  createClient as createSSEClient,
  ClientOptions,
  Client
} from 'graphql-sse';

import WebSocket from 'ws';
import fetch from 'cross-fetch';
import { HttpLink } from '@apollo/client/link/http';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { split } from '@apollo/client/link/core';
import { createClient as createWSClient } from 'graphql-ws';
import { getMainDefinition } from '@apollo/client/utilities';

export class SSELink extends ApolloLink {
  private client: Client;

  constructor(options: ClientOptions) {
    super();
    this.client = createSSEClient(options);
  }

  public request(operation: Operation): Observable<FetchResult> {
    return new Observable((sink) => {
      return this.client.subscribe<FetchResult>(
        { ...operation, query: print(operation.query) },
        {
          next: sink.next.bind(sink),
          complete: sink.complete.bind(sink),
          error: sink.error.bind(sink)
        }
      );
    });
  }
}

const wsClient = createWSClient({
  url: 'ws://localhost:4000',
  webSocketImpl: WebSocket
});

export const http = new HttpLink({
  uri: 'http://localhost:4000',
  fetch
});

export const websocket = new GraphQLWsLink(wsClient);

export const sse = new SSELink({
  url: 'http://localhost:4000',
  headers: () => {
    return { Authorization: 'test' };
  }
});

export function createSplitLink(query: ApolloLink, subscription: ApolloLink) {
  return split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    subscription,
    query
  );
}
