import { gql } from 'graphql-tag';

import { Client } from './utils/client';
import { makeSchema } from '../../lib/schema';

let client: Client;

beforeAll(async () => {
  client = new Client({ schema: await makeSchema() });
});

test('query', async () => {
  const result: any = await client.query({
    query: gql`
      {
        hello
      }
    `
  });
  expect(result.data.hello).toEqual('Hello');
});

test('mutation', async () => {
  const result: any = await client.mutate({
    query: gql`
      mutation {
        publish
      }
    `
  });
  expect(result.data.publish).toEqual('value');
});

test('subscription generator', async () => {
  const result: any = await client.subscribe({
    query: gql`
      subscription {
        greetings
      }
    `
  });
  expect(result.data.greetings).toEqual('Hi');
});

test('subscription pubsub', async () => {
  const result: any = await client.subscribe({
    query: gql`
      subscription {
        subscribe
      }
    `,
    trigger: async () => {
      await client.mutate({
        query: gql`
          mutation {
            publish
          }
        `
      });
    }
  });
  expect(result.data.subscribe).toEqual('value');
});
