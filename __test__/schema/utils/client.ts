import { execute, subscribe, DocumentNode, GraphQLSchema } from 'graphql';
import { context } from '../../../lib/context';

interface ClientArguments {
  schema: GraphQLSchema;
}

interface QueryArguments {
  query: DocumentNode;
}

interface SubscriptionArguments extends QueryArguments {
  trigger?: () => void;
}

export class Client {
  schema: GraphQLSchema;
  context: (request: any, context: any) => Promise<any>;

  constructor(args: ClientArguments) {
    this.schema = args.schema;
    this.context = context;
  }

  async query(args: QueryArguments) {
    return await execute({
      document: args.query,
      schema: this.schema,
      contextValue: await this.context(null, {})
    });
  }

  async mutate(args: QueryArguments) {
    return await execute({
      document: args.query,
      schema: this.schema,
      contextValue: await this.context(null, {})
    });
  }

  async subscribe(args: SubscriptionArguments) {
    const subscription: any = await subscribe({
      document: args.query,
      schema: this.schema,
      contextValue: await this.context(null, {})
    });
    const result = subscription.next();
    if (args.trigger) {
      args.trigger();
    }
    return (await result).value;
  }
}
