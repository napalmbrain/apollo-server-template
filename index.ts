import { env, pid } from 'process';
import cluster from 'cluster';
import { cpus } from 'os';

import { port } from './lib/constants';
import { apollo } from './lib/server';

switch (env.ENVIRONMENT) {
  case 'development':
    (async () => {
      await apollo({ port });
      console.log(`PID ${pid}: Server started at port ${port}.`);
    })();
    break;
  case 'production':
    (async () => {
      if (cluster.isPrimary) {
        console.log(`PID ${pid}: Primary started.`);
        for (let i = 0; i < cpus().length; i++) {
          cluster.fork();
        }
      } else {
        await apollo({ port });
        console.log(`PID ${pid}: Worker started at port ${port}.`);
      }
    })();
    break;
  default:
    console.error(
      'No environment provided: export ENVIRONMENT="development|production"'
    );
}
