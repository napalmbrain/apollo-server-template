import path from 'path';
import { promisify } from 'util';
import glob from 'glob';

const globber = promisify(glob);

interface IModule {
  default: any;
}

export async function* importModules(
  pattern: string
): AsyncGenerator<IModule, any, any> {
  const files = await globber(path.resolve(pattern));
  for await (const file of files) {
    const module = await import(file.toString());
    yield module;
  }
}

export async function importResolvers(pattern: string): Promise<any[]> {
  const resolvers: Array<IModule> = [];
  const modules = importModules(pattern);
  for await (const module of modules) {
    resolvers.push(module.default);
  }
  return resolvers;
}
