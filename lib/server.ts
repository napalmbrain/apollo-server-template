import { createServer } from 'http';
import { createHandler } from 'graphql-sse';

import { ApolloServer } from 'apollo-server';
import { WebSocketServer } from 'ws';
import { useServer } from 'graphql-ws/lib/use/ws';

import { context } from './context';
import { makeSchema } from './schema';

export async function sse({ port = 4000 }) {
  const schema = await makeSchema();

  const sse = createHandler({
    schema,
    context
  });

  const http = createServer(async (request, response) => {
    try {
      await sse(request, response);
    } catch (error) {
      console.error(error);
      if (!response.headersSent) {
        response.writeHead(500, 'Internal Server Error').end();
      }
    }
  });

  http.listen(port);

  return { server: { http } };
}

export async function apollo({ port = 4000 }) {
  const schema = await makeSchema();

  const apollo = new ApolloServer({
    schema,
    context,
    cors: true
  });

  const { url, server: http } = await apollo.listen({ port });
  const ws = new WebSocketServer({ server: http });

  useServer({ schema, context }, ws);

  return { url, server: { http, ws } };
}
