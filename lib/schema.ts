import { loadFiles as importTypeDefs } from '@graphql-tools/load-files';
import { importResolvers } from './utils';
import { mergeTypeDefs, mergeResolvers } from '@graphql-tools/merge';
import { makeExecutableSchema } from '@graphql-tools/schema';

const typeDefs = importTypeDefs('./typedefs/**/*.graphql').then(mergeTypeDefs);
const resolvers = importResolvers('./resolvers/**/*.+(t|j)s').then(
  mergeResolvers
);

export async function makeSchema() {
  return makeExecutableSchema({
    typeDefs: await typeDefs,
    resolvers: await resolvers
  });
}
