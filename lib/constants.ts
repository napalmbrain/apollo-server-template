import { env } from 'process';
import { PubSub } from 'graphql-subscriptions';
import { RedisPubSub } from 'graphql-redis-subscriptions';

export const port = env.PORT ? Number(env.PORT) : 4000;
export const pubsub =
  env.ENVIRONMENT === 'production' ? new RedisPubSub() : new PubSub();
