import { pubsub } from '../../lib/constants';

export default {
  Subscription: {
    greetings: {
      resolve(payload: any, args: any, context: any, info: any) {
        return payload.greetings;
      },
      async *subscribe(root: any, args: any, context: any, info: any) {
        for (const hi of ['Hi', 'Bonjour', 'Hola', 'Ciao', 'Zdravo']) {
          yield { greetings: hi };
        }
      }
    },
    /*subscribe: {
      subscribe(root: any, args: any, context: any, info: any) {
        return pubsub.asyncIterator('pubsub');
      }
    }*/
    subscribe: {
      async *subscribe(root: any, args: any, context: any, info: any) {
        const iterator = pubsub.asyncIterator('pubsub');
        while (true) {
          const result = await iterator.next();
          yield result.value;
          //await iterator.return?.();
          //return;
        }
      }
    }
  }
};
